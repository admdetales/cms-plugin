<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Tests\Functional\Manager;

use Lakion\ApiTestCase\JsonApiTestCase;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;

class NodeRelationManagerTest extends JsonApiTestCase
{
    public function testGetRelation()
    {
        $this->loadFixturesFromFile('cms.yml');

        /** @var NodeInterface $node */
        $node = $this->get('omni_sylius.repository.node')->findOneBy(['type' => 'taxon']);
        $taxon = $this->get('omni_sylius.manager.node_relation')->getRelation($node);

        $this->assertEquals($node->getRelationId(), $taxon->getId());
    }
}
