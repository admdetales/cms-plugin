# Sylius CMS plugin

![Node tree](docs/img/node_tree.png?raw=true "Node Tree")

A plugin that allows the formation of static content and the formation
of menus for sylius.

## Installation

### Require it via composer
 
 ```bash

 $ composer require omni/sylius-cms-plugins
 ```
 
### Add the plugin to Kernel

```php
# AppKernel.php

public function registerBundles()
{
  $bundles = [
    // ...
    new \Omni\Sylius\CmsPlugin\OmniSyliusCmsPlugin(),
    // ...
  ];
}
```

### Add routing

```yaml
# routing.yml

sylius_cms_routes_front:
    resource: "@OmniSyliusCmsPlugin/Resources/config/routing_front.yml"
    prefix: /content
    
sylius_cms_routes:
    resource: "@OmniSyliusCmsPlugin/Resources/config/routing.yml"
    prefix: /admin
```

> Disclamer: the routes should be added to the top of the file, otherwise some of them 
might be interpreted with sylius routes and fail to be found.
 
### Configuration

This section determines how the plugin will behave and what it will actually be able 
to do. The most important thing is to configure the `node_types`. Depending on node 
types that you require the configuration will differ. Here is the example configuration:
 
```yaml
omni_sylius_cms:
    node_types:
        content:                   # node type name
            content: true          # node configuration
            multiple: true
        brand:
            content: true
            layout: true # node layout exist
            multiple: true
            file_upload: true      # deprecated, use `images` config
        taxon:
            relation: sylius.taxon # sylius resource to relate to
            multiple: true
        fancy_images_node:
            relation: omni_sylius.node
            multiple: true
            images:                # this manages images associated with the node
                enabled: true
                linkable: true     # default false
                title_aware: true
                available_positions:
                    - left
                    - center_left
                    - center_right
                    - right
                relations:
                    - sylius.taxon
                    - omni_sylius.node
        placeholder:
            nestable: true
            multiple: true
        main_menu:
            nestable: true
        footer_menu:
            nestable: true
```

Or if you want you can simply import standard configuration from the plugin to your `config.yml`
file:

```yaml
# app/config/config.yml

imports:
    - { resource: "@OmniSyliusCmsPlugin/Resources/config/config.yml" }
```

You can read more on configuration of the bundle in the section Node configuration bellow

### Fixtures

If you include the standard configuration file of the bundle then the fixture nodes for 
the bundle can be loaded along other sylius fixtures by running this command:

```bash
$ bin/console sylius:fixtures:load
```

> Notice that this action purges the database

### Default templates

It is very likely that for any project you will be overriding all the default templates, however
just to get a feel for dynamic top menu and footer creation, you can use the example templates from
the plugin itself. This means that creating a dynamic top menu and footer with omni sylius cms plugin
is as easy as including two templates:

```twig
{# app/Resources/SyliusShopBundle/views/Taxon/_horizontalMenu.html.twig #}

{% include '@OmniSyliusCmsPlugin/Example/Taxon/_horizontalMenu.html.twig' %}
```

```twig
{# app/Resources/SyliusShopBundle/views/_footer.html.twig #}

{% include '@OmniSyliusCmsPlugin/Example/_footer.html.twig' %}
```

And that's it, try to update something in the node tree of the Admin panel and your changes will
have an effect on your menus.

## Usage

### Basics

This bundle provides the `Node` model for menu and content formation. After you install the plugin
you can see the `Node tree` link in the `Manage nodes` section in the left sidebar of your 
admin panel. You can see it in the bottom-left corner of the image above. It is used for the 
formation of the node tree structure that can later be fetched in the templates to form menus 
or other tree type structures. Each individual node can serve as a relation to other resources
or even have content of its own.

A node can be just an element of the tree used for structuring other elements and not hold any
content by itself (such nodes should have `nestable = true` and `content = false` set) or they 
can be rendered as separate static page (in this case `content = true` should be set). A node
can also serve as a link to a different resource with the help of the `relation` parameter. You 
could take a look at the example tree above to get the idea, for example, all the nodes in the 
`main menu` top level node are simply relations to corresponding taxons. You should try out these
 different nodes and you will soon get the hang of it. Also, for your convenience the plugin comes
 with fixtures of the very same node tree depicted at the beginning of the documentation.

A node can be fetched in the templates using `omni_sylius_get_node_by_type()` twig function.
This is of course only valid for nodes that have unique types (`multiple: false` in node config).
Such nodes are used as top level arrangement units. All nodes that are within the node tree
of this node can be accessed via its `children` attribute.

For maximum flexibility, nodes can be configured to have content, images with different types,
relations to other resources and much more.

You can read more on that in the next section.

### Node configuration

This section provides the full description of the nodes config parameters:

| Setting name  | Type     | Default   | Meaning                                                                  |
|---------------|----------|-----------|--------------------------------------------------------------------------|
| `multiple`    | `bool`   | `false`   | If multiple is set to false there can only be a single node of such type |
| `content`     | `bool`   | `false`   | Enables the translations for the node and creating content               |
| `layout`      | `bool`   | `false`   | When enabled, you can get all layoutable nodes array from this parameter `omni_sylius_cms.layoutable.nodes`|
| `scopable`    | `bool`   | `false`   | Some node types can be configured to have channels assigned to them      |
| `nestable`    | `bool`   | `false`   | Enables the node to have children                                        |
| `relation`    | `string` |     -     | Relates the node to a given sylius resource                              |
| `file_upload` | `bool`   | `false`   | Enables uploading images to be associated with the node  (!!!!deprecated)|
| `template`    | `string` |     -     | Used for altering node display                                           |        
| `images`      | `array`  |     -     | Manages images association with nodes. read more bellow                  |        

If we would examine the example configuration provided in the configuration section with
the description above we would see that there are two non-multiple nodes configured: `main_menu` and `footer_menu` 
that can contain child nodes. These nodes act as top level nodes used for the formation of different
menus. Usually they will be nested with the nodes that have the type `placeholder`. These nodes are multiple,
but they do not have the content, they can only contain low level nodes such as `content`, `brand` or `taxon`
node types. `taxon` node type is special in a way that it is related to the resource. More on that in the 
next section. Please have in mind that the naming of the node types is completely arbitrary and you can
name them whatever you like.

> Please note that `file_upload` is deprecated and will be removed in the next stable release.
Use `images` config in stead!

### Relations

Every node type can be configured to relate to a single sylius resource. This is done by
providing the node parameter `relation` with the id of the sylius resource like so:

```yaml
# ...
taxon:
   relation: sylius.taxon
   # ...
# ...

```
It is very important to mention that not any kind of resource can be configured here.
When adding a relation please make sure that the relation implements `TranslatableInterface`
and that the translation of the node has either `getName` or `getTitle` method that will be
used for the display of the relation label. Relation of the node can be retrieved in template
with the help of `omni_sylius_get_node_relation($node)` twig function. More on twig functions
in the next section.

### Images

When building menus it often becomes important to add certain images with links in certain places of 
the menu. Images are also important when building static content pages, such as about, read more, or 
blog pages. This is why we introduced the possibility of assigning images to nodes and went nuts on 
the details of it. This is how it works:

#### 1. Simple images

If you simply need images at your node, just enable them in the config:
 
```yaml
# ...
your_node_type:
    # ...
    images:      
        enabled: true
```

This way your images will simply have a file associated with them and a type for easier handling if 
the amount of images were to grow and you ever need a way to organize them better.

#### 2. Named images

Images can have a title, it is not very common, but not unusual either, so if you need a localized 
title for the images just enable it:

```yaml
# ...
your_node_type:
    # ...
    images:      
        enabled: true
        title_aware: true
```

#### 3. Image positions

If the `type` parameter of the image is not enough and you still can't find a way to organize your 
images in an acceptable manner then have no fear, because at a touch of configuration file you can 
assign positions to your node images. You can configure any positions you like just so:

```yaml
# ...
your_node_type:
    # ...
    images:      
        enabled: true
        available_positions:
            - left
            - center_left
            - center_right
            - right
```

And a select box will be presented to you whenever you will be creating node images.

#### 4. Image relations

Remember Node relations? Well this is exactly the same thing only better! It's better because every
image of the node can be related to a different resource and you can configure available resources for 
every node seperately, ain't that neat? This is how it works: just add configuration:

```yaml
# ...
your_node_type:
    # ...
    images:      
        enabled: true
        relations:
            - sylius.taxon
            - omni_sylius.node
```

And now when you're configuring images, you will be able to select the resource type and search for the
exact resource entity to relate the image to. This is how the full node image formation looks like:

![Image editing](docs/img/image_editing.png?raw=true "Image editing")

Alternatively, you can use `linkable: true` in your image configuration. This will
provide an opportunity to add concrete links to the translations of your images. Using
this enables linking your images to absolutely anything, however the validity of these links will
not be handled in any way, therefore managing them is entirely up to you. Furthermore,
if you should choose to add both relations and links to your images, the prioritizing
between these functionalities should be done on your behalf in the rendering
of the front end of the application (twig).

### Twig functions:

In twig templates you can use these functions:

| Function                              | Argument           | Meaning                                                  |
|---------------------------------------|--------------------|----------------------------------------------------------|
| `omni_sylius_is_node_nestable`        | Node               | Returns true if node can have children                   |
| `omni_sylius_is_node_related`         | Node               | Returns true if node can have relation                   |
| `omni_sylius_is_node_scopable`        | Node               | Returns true if node can have channels assigned to it    |
| `omni_sylius_is_node_upload_aware`    | Node               | Returns true if node images can be uploaded (deprecated) |
| `omni_sylius_is_node_image_aware`     | Node               | Returns true if node images can be uploaded              |
| `omni_sylius_get_node_by_type`        | Type (string)      | Returns a single node by its type                        |
| `omni_sylius_get_node_relation`       | Node               | Returns the relation of the node (deprecated)            |
| `omni_sylius_get_relation`            | RelatableInterface | Returns the relation of the node                         |
| `omni_sylius_get_relation_label`      | Node relation      | Returns the label of the relation                        |
| `omni_sylius_get_node_image_config`   | Node               | Returns the entire node images configuration             |
| `omni_sylius_get_node_image_positions`| Node               | Returns the available positions of all the images        |

> Note that `omni_sylius_get_node_relation($node)` should always be preferred to `$node->getRelation()`
 because the relation could be not loaded to the node yet!

> Note that since v1.1 the RelatableInterface was introduced and it is prefferred to call abstract functions
(like `omni_sylius_get_relation` for example) in stead of those associated solely with node relations, because
all the latter ones will be removed in v2.0   
