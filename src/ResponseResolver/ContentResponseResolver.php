<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\ResponseResolver;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class ContentResponseResolver implements ResponseResolverInterface
{
    /** @var NodeTypeManager */
    private $typeManager;

    /** @var UrlGeneratorInterface */
    private $router;

    /** @var HttpKernelInterface */
    private $httpKernel;

    /**
     * @param NodeTypeManager $typeManager
     * @param UrlGeneratorInterface $router
     * @param HttpKernelInterface $httpKernel
     */
    public function __construct(
        NodeTypeManager $typeManager,
        UrlGeneratorInterface $router,
        HttpKernelInterface $httpKernel
    ) {
        $this->typeManager = $typeManager;
        $this->httpKernel = $httpKernel;
        $this->router = $router;
    }

    /**
     * @param Request $request
     * @param NodeInterface $node
     *
     * @return bool
     */
    public function supports(Request $request, NodeInterface $node): bool
    {
        return $this->typeManager->hasContent($node->getType())
            && $this->typeManager->isSlugable($node->getType());
    }

    /**
     * @param Request $request
     * @param NodeInterface $node
     *
     * @return Response
     *
     * @throws HttpExceptionInterface
     */
    public function getResponse(Request $request, NodeInterface $node): Response
    {
        $server = $request->server->all();
        $server['REQUEST_METHOD'] = Request::METHOD_GET;
        $server['REQUEST_URI'] = $this->router->generate('omni_sylius_cms_frontend_show', ['slug' => $node->getSlug()]);

        $subRequest = new Request([], [], [], $request->cookies->all(), [], $server);
        $subRequest->setMethod(Request::METHOD_GET);

        return $this->httpKernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
    }
}
