<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\UrlResolver;

use Omni\Sylius\CmsPlugin\Model\NodeInterface;

final class CompositeUrlResolver implements UrlResolverInterface
{
    /** @var UrlResolverInterface[] */
    private $resolvers;

    /**
     * @param UrlResolverInterface[] $resolvers
     */
    public function __construct(iterable $resolvers)
    {
        $this->resolvers = $resolvers;
    }

    public function supports(NodeInterface $node): bool
    {
        foreach ($this->resolvers as $resolver) {
            if ($resolver->supports($node)) {
                return true;
            }
        }

        return false;
    }

    public function getNodeUrl(NodeInterface $node): NodeUrl
    {
        foreach ($this->resolvers as $resolver) {
            if ($resolver->supports($node)) {
                return $resolver->getNodeUrl($node);
            }
        }

        return new NodeUrl('');
    }
}
