<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\UrlResolver;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class SlugableUrlResolver implements UrlResolverInterface
{
    /** @var NodeTypeManager */
    private $typeManager;

    /** @var UrlGeneratorInterface */
    private $router;

    /**
     * @param NodeTypeManager $typeManager
     * @param UrlGeneratorInterface $router
     */
    public function __construct(
        NodeTypeManager $typeManager,
        UrlGeneratorInterface $router
    ) {
        $this->typeManager = $typeManager;
        $this->router = $router;
    }

    /**
     * @param NodeInterface $node
     *
     * @return bool
     */
    public function supports(NodeInterface $node): bool
    {
        return false === $node->isSlugFromRelation() && $this->typeManager->isSlugable($node->getType());
    }

    /**
     * @param NodeInterface $node
     *
     * @return NodeUrl
     */
    public function getNodeUrl(NodeInterface $node): NodeUrl
    {
        if (empty($node->getSlug())) {
            return new NodeUrl('');
        }

        return new NodeUrl($this->router->generate('omni_sylius_cms_node_show', ['slug' => $node->getSlug()]));
    }
}
