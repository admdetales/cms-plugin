<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Omni\Sylius\SeoPlugin\Model\SeoAwareTrait;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;
use Sylius\Component\Resource\Model\ToggleableTrait;
use Sylius\Component\Resource\Model\TranslatableTrait;

/**
 * @method NodeTranslationInterface getTranslation(?string $locale = null)
 */
class Node implements NodeInterface
{
    use TimestampableTrait, ToggleableTrait, SeoAwareTrait, RelatableTrait;
    use TranslatableTrait {
        __construct as private initializeTranslationsCollection;
    }

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var int
     */
    protected $left;

    /**
     * @var int
     */
    protected $level;

    /**
     * @var int
     */
    protected $right;

    /**
     * @var NodeInterface
     */
    protected $root;

    /**
     * @var null|NodeInterface
     */
    protected $parent;

    /**
     * @var ArrayCollection|NodeInterface[]
     */
    protected $children;

    /**
     * @var ArrayCollection|ImageInterface[]|NodeImageInterface[]
     */
    protected $images;

    /**
     * @var int|null
     */
    protected $position;

    /**
     * @var ChannelInterface[]|Collection
     */
    protected $channels;

    /**
     * @var bool
     */
    protected $slugFromRelation = false;

    /**
     * Node constructor.
     */
    public function __construct()
    {
        $this->initializeTranslationsCollection();

        $this->createdAt = new \DateTime();
        $this->children = new ArrayCollection();
        $this->channels = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * {@inheritdoc}
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    public function setType(?string $type): NodeInterface
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Needed for serialization related reasons
     *
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->getTranslation()->getTitle();
    }

    /**
     * {@inheritdoc}
     */
    public function getLeft(): ?int
    {
        return $this->left;
    }

    /**
     * {@inheritdoc}
     */
    public function setLeft(?int $left): NodeInterface
    {
        $this->left = $left;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLevel(): ?int
    {
        return $this->level;
    }

    /**
     * {@inheritdoc}
     */
    public function setLevel(?int $level): NodeInterface
    {
        $this->level = $level;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRight(): ?int
    {
        return $this->right;
    }

    /**
     * {@inheritdoc}
     */
    public function setRight(?int $right): NodeInterface
    {
        $this->right = $right;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoot(): ?NodeInterface
    {
        return $this->root;
    }

    /**
     * {@inheritdoc}
     */
    public function setRoot(NodeInterface $root): NodeInterface
    {
        $this->root = $root;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): ?NodeInterface
    {
        return $this->parent;
    }

    /**
     * {@inheritdoc}
     */
    public function setParent(?NodeInterface $parent): NodeInterface
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * {@inheritdoc}
     */
    public function hasChildren(): bool
    {
        return (bool) count($this->children);
    }

    /**
     * {@inheritdoc}
     */
    public function setChildren(Collection $children): NodeInterface
    {
        $this->children = $children;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addChild(NodeInterface $child): NodeInterface
    {
        if (false === $this->children->contains($child)) {
            $child->setParent($this);
            $this->children->add($child);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeChild(NodeInterface $child): NodeInterface
    {
        if ($this->children->contains($child)) {
            $child->setParent(null);
            $this->children->removeElement($child);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * {@inheritdoc}
     */
    public function getImagesByType(string $type): Collection
    {
        return $this->images->filter(function (ImageInterface $image) use ($type) {
            return $type === $image->getType();
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getImagesByPosition(string $position): Collection
    {
        return $this->images->filter(function (NodeImageInterface $image) use ($position) {
            return $position === $image->getPosition();
        });
    }

    /**
     * {@inheritdoc}
     */
    public function hasImages(): bool
    {
        return !$this->images->isEmpty();
    }

    /**
     * {@inheritdoc}
     */
    public function hasImage(ImageInterface $image): bool
    {
        return $this->images->contains($image);
    }

    /**
     * {@inheritdoc}
     */
    public function addImage(ImageInterface $image): void
    {
        if (false === $this->hasImage($image)) {
            $image->setOwner($this);
            $this->images->add($image);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeImage(ImageInterface $image): void
    {
        $image->setOwner(null);
        $this->images->removeElement($image);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->getId();
    }

    /**
     * @return bool
     */
    public function isSlugFromRelation(): bool
    {
        return $this->slugFromRelation;
    }

    /**
     * @param bool $slugFromRelation
     *
     * @return $this
     */
    public function setSlugFromRelation(bool $slugFromRelation)
    {
        $this->slugFromRelation = $slugFromRelation;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function createTranslation()
    {
        return new NodeTranslation();
    }

    /**
     * @return string
     */
    public function getLink(): ?string
    {
        return $this->getTranslation()->getLink();
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     */
    public function setPosition(?int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return Collection|ChannelInterface[]
     */
    public function getChannels(): Collection
    {
        return $this->channels;
    }

    /**
     * @param ChannelInterface $chanels
     *
     * @return Node
     */
    public function removeChannel(ChannelInterface $channel): NodeInterface
    {
        $this->channels->removeElement($channel);

        return $this;
    }

    /**
     * @param ChannelInterface $chanels
     *
     * @return Node
     */
    public function addChannel(ChannelInterface $channel): NodeInterface
    {
        if (!$this->channels->contains($channel)) {
            $this->channels->add($channel);
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->getTranslation()->getSlug();
    }

    public function getLinkTarget(): ?string
    {
        return $this->getTranslation()->getLinkTarget();
    }

    public function getTitle(): ?string
    {
        return $this->getTranslation()->getTitle();
    }

    public function hasEnabledChildren(): bool
    {
        foreach ($this->getChildren() as $child) {
            if ($child->isEnabled()) {
                return true;
            }
        }

        return false;
    }
}
