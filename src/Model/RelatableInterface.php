<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Model;

use Sylius\Component\Resource\Model\ResourceInterface;

interface RelatableInterface
{
    /**
     * @return string
     */
    public function getRelationType();

    /**
     * @param string $relationType
     *
     * @return NodeInterface
     */
    public function setRelationType($relationType);

    /**
     * @return int
     */
    public function getRelationId();

    /**
     * @param int $relationId
     *
     * @return NodeInterface
     */
    public function setRelationId($relationId);

    /**
     * @return ResourceInterface
     */
    public function getRelation(): ?ResourceInterface;

    /**
     * @param ResourceInterface $relation
     *
     * @return NodeInterface
     */
    public function setRelation(?ResourceInterface $relation);
}
