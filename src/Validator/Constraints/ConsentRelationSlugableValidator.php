<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Validator\Constraints;

use Sylius\Component\Resource\Model\SlugAwareInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class ConsentRelationSlugableValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof ConsentRelationSlugable) {
            throw new UnexpectedTypeException($constraint, ConsentRelationSlugable::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (
            !$value instanceof TranslatableInterface ||
            !$value->getTranslation() instanceof SlugAwareInterface ||
            !$value->getTranslation()->getSlug()
        ) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
