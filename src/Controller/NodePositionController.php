<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types = 1);

namespace Omni\Sylius\CmsPlugin\Controller;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class NodePositionController extends AbstractController
{
    const UP = 'up';
    const DOWN = 'down';

    /**
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function moveUpAction(Request $request, int $id)
    {
        /** @var NodeInterface $sibling */
        $node = $this->get('omni_sylius.repository.node')->find($id);

        try {
            if (!$node->getParent()) {
                $this->swapRootNodesPosition($node, self::UP);
            } else {
                $this->get('omni_sylius.repository.node')->moveUp($node, 1);
            }
        } catch (\Exception $e) {
            return new JsonResponse('Could not move node: ' . $e->getMessage());
        }

        return new JsonResponse('Node was moved successfully');
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function moveDownAction(Request $request, int $id)
    {
        /** @var NodeInterface $sibling */
        $node = $this->get('omni_sylius.repository.node')->find($id);

        try {
            if (!$node->getParent()) {
                $this->swapRootNodesPosition($node, self::DOWN);
            } else {
                $this->get('omni_sylius.repository.node')->moveDown($node, 1);
            }
        } catch (\Exception $e) {
            return new JsonResponse('Could not move node: ' . $e->getMessage());
        }

        return new JsonResponse('Node was moved successfully');
    }

    /**
     * @param NodeInterface $node
     * @param string $destination
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function swapRootNodesPosition(NodeInterface $node, string $destination): void
    {
        $orderBy = ($destination === self::UP) ? 'DESC' : 'ASC';
        $rootNodes = $this->get('omni_sylius.repository.node')->findRootNodes($orderBy);

        foreach ($rootNodes as $siblingNode) {
            if (null === $siblingPosition = $siblingNode->getPosition()) {
                return;
            }

            if ($this->NodesPositionSwapByDestination($node, $siblingNode, $destination, $siblingPosition)) {
                $this->persistChanges($node, $siblingNode);
                break;
            };
        }
    }

    /**
     * @param mixed ...$nodes
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function persistChanges(...$nodes): void
    {
        $em = $this->get('doctrine.orm.entity_manager');

        foreach ($nodes as $node) {
            $em->persist($node);
        }

        $em->flush();
    }

    /**
     * @param NodeInterface $node
     * @param NodeInterface $siblingNode
     * @param string $destination
     * @param int $siblingPosition
     *
     * @return bool
     */
    protected function NodesPositionSwapByDestination(
        NodeInterface $node,
        NodeInterface $siblingNode,
        string $destination,
        int $siblingPosition
    ): bool {
        if ($this->isPositionChangeNeeded($node, $destination, $siblingPosition)) {
            $siblingNode->setPosition($node->getPosition());
            $node->setPosition($siblingPosition);

            return true;
        }

        return false;
    }

    /**
     * @param NodeInterface $node
     * @param string $destination
     * @param int $siblingPosition
     *
     * @return bool
     */
    protected function isPositionChangeNeeded(NodeInterface $node, string $destination, int $siblingPosition): bool
    {
        return ($destination === self::UP && $node->getPosition() > $siblingPosition)
        || ($destination === self::DOWN && $node->getPosition() < $siblingPosition);
    }
}
