<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Form\Type;

use Omni\Sylius\CmsPlugin\Form\Subscriber\AddContentSubscriber;
use Omni\Sylius\CmsPlugin\Form\Subscriber\AddLinkSubscriber;
use Omni\Sylius\CmsPlugin\Form\Subscriber\SeoMetadataSubscriber;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class NodeTranslationType extends AbstractResourceType
{
    /**
     * @var AddContentSubscriber
     */
    private $addContentSubscriber;

    /**
     * @var SeoMetadataSubscriber
     */
    private $seoMetadataSubscriber;

    /**
     * @var AddLinkSubscriber
     */
    private $addLinkSubscriber;

    /**
     * @param string                $dataClass
     * @param array                 $validationGroups
     * @param AddContentSubscriber  $addContentSubscriber
     * @param SeoMetadataSubscriber $seoMetadataSubscriber
     * @param AddLinkSubscriber     $addLinkSubscriber
     */
    public function __construct(
        string $dataClass,
        array $validationGroups,
        AddContentSubscriber $addContentSubscriber,
        SeoMetadataSubscriber $seoMetadataSubscriber,
        AddLinkSubscriber $addLinkSubscriber
    ) {
        parent::__construct($dataClass, $validationGroups);

        $this->addContentSubscriber = $addContentSubscriber;
        $this->seoMetadataSubscriber = $seoMetadataSubscriber;
        $this->addLinkSubscriber = $addLinkSubscriber;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'omni_sylius.form.node.title',
            ])
            ->addEventSubscriber($this->addContentSubscriber)
            ->addEventSubscriber($this->seoMetadataSubscriber)
            ->addEventSubscriber($this->addLinkSubscriber)
        ;
    }
}
