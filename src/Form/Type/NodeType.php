<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Form\Type;

use Omni\Sylius\CmsPlugin\Form\Subscriber\AddChannelsSubscriber;
use Omni\Sylius\CmsPlugin\Form\Subscriber\AddImagesFieldSubscriber;
use Omni\Sylius\CmsPlugin\Form\Subscriber\AddParentFieldSubscriber;
use Omni\Sylius\CmsPlugin\Form\Subscriber\AddRelationSubscriber;
use Omni\Sylius\CmsPlugin\Form\Subscriber\AddTypeChoiceSubscriber;
use Omni\Sylius\CmsPlugin\Form\Subscriber\RemoveTypeSubscriber;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class NodeType extends AbstractResourceType
{
    /**
     * @var AddParentFieldSubscriber
     */
    private $addParentFieldSubscriber;

    /**
     * @var AddRelationSubscriber
     */
    private $addRelationSubscriber;

    /**
     * @var AddImagesFieldSubscriber
     */
    private $addImagesFieldSubscriber;

    /**
     * @var AddTypeChoiceSubscriber
     */
    private $addTypeChoiceSubscriber;

    /**
     * @var AddChannelsSubscriber
     */
    private $addChannelsSubscriber;

    /**
     * @var RemoveTypeSubscriber
     */
    private $removeTypeSubscriber;

    /**
     * NodeType constructor.
     *
     * @param string                   $dataClass
     * @param array                    $validationGroups
     * @param AddParentFieldSubscriber $addParentFieldSubscriber
     * @param AddRelationSubscriber    $addRelationSubscriber
     * @param AddImagesFieldSubscriber $addImagesFieldSubscriber
     * @param AddTypeChoiceSubscriber  $addTypeChoiceSubscriber
     * @param AddChannelsSubscriber    $addChannelsSubscriber
     * @param RemoveTypeSubscriber     $removeTypeSubscriber
     */
    public function __construct(
        $dataClass,
        array $validationGroups,
        AddParentFieldSubscriber $addParentFieldSubscriber,
        AddRelationSubscriber $addRelationSubscriber,
        AddImagesFieldSubscriber $addImagesFieldSubscriber,
        AddTypeChoiceSubscriber $addTypeChoiceSubscriber,
        AddChannelsSubscriber $addChannelsSubscriber,
        RemoveTypeSubscriber $removeTypeSubscriber
    ) {
        parent::__construct($dataClass, $validationGroups);

        $this->addParentFieldSubscriber = $addParentFieldSubscriber;
        $this->addRelationSubscriber = $addRelationSubscriber;
        $this->addImagesFieldSubscriber = $addImagesFieldSubscriber;
        $this->addTypeChoiceSubscriber = $addTypeChoiceSubscriber;
        $this->addChannelsSubscriber = $addChannelsSubscriber;
        $this->removeTypeSubscriber = $removeTypeSubscriber;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', TextType::class, [
                'label' => 'omni_sylius.form.node.code',
            ])
            ->add('enabled', CheckboxType::class, [
                'required' => false,
                'label' => 'sylius.form.product.enabled',
            ])
            ->add('translations', ResourceTranslationsType::class, [
                'entry_type' => NodeTranslationType::class,
                'label' => 'omni_sylius.form.node.translations',
            ])
            ->addEventSubscriber($this->addParentFieldSubscriber)
            ->addEventSubscriber($this->addRelationSubscriber)
            ->addEventSubscriber($this->addImagesFieldSubscriber)
            ->addEventSubscriber($this->addTypeChoiceSubscriber)
            ->addEventSubscriber($this->addChannelsSubscriber)
            ->addEventSubscriber($this->removeTypeSubscriber)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'omni_sylius_node';
    }
}
