<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Form\Subscriber;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Manager\RelationManager;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AddRelationSubscriber implements EventSubscriberInterface
{
    /**
     * @var NodeTypeManager
     */
    private $handler;

    /**
     * @var RelationManager
     */
    private $relationManager;

    /**
     * @param NodeTypeManager $handler
     */
    public function __construct(NodeTypeManager $handler, RelationManager $relationManager)
    {
        $this->handler = $handler;
        $this->relationManager = $relationManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::SUBMIT => 'onSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();

        if (false === $data instanceof NodeInterface || false === $this->handler->isRelated($data->getType())) {
            return;
        }

        $this->relationManager->getRelation($data);

        $event->getForm()->add(
            'relation',
            EntityType::class,
            [
                'label' => 'omni_sylius.form.node.relation',
                'class' => $this->handler->getRelationMetadata($data->getType())['class'],
                'choice_label' => function (TranslatableInterface $relation) {
                    return $this->relationManager->getRelationLabel($relation);
                },
                'required' => false,
            ]
        );

        if (false === $this->handler->isSlugable($data->getType())) {
            return;
        }

        $event->getForm()->add(
            'slugFromRelation',
            CheckboxType::class,
            [
                'label' => 'omni_sylius.form.node.slug_from_relation',
                'required' => false,
            ]
        );
    }

    /**
     * @param FormEvent $event
     */
    public function onSubmit(FormEvent $event)
    {
        /** @var NodeInterface $node */
        $node = $event->getData();

        if (null !== $relation = $node->getRelation()) {
            $node->setRelationId($relation->getId());
            $node->setRelationType($this->handler->getRelationMetadata($node->getType())['alias']);
        }
    }
}
