<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Form\Subscriber;

use Doctrine\ORM\EntityRepository;
use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\Node;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AddParentFieldSubscriber implements EventSubscriberInterface
{
    /**
     * @var NodeTypeManager
     */
    private $nodeManager;

    /**
     * @param NodeTypeManager $nodeManager
     */
    public function __construct(NodeTypeManager $nodeManager)
    {
        $this->nodeManager = $nodeManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData (FormEvent $event)
    {
        $node = $event->getData();

        if (false === $this->isParentFieldRequired($node)) {
            return;
        }

        $event->getForm()->add('parent', EntityType::class, [
            'label' => 'omni_sylius.form.node.parent',
            'class' => Node::class,
            'query_builder' => function (EntityRepository $er) use ($node) {
                $qb = $er->createQueryBuilder('u');

                return $qb
                    ->andWhere($qb->expr()->neq('u.id', ':id'))
                    ->andWhere($qb->expr()->in('u.type', ':nestable_types'))
                    ->setParameter('id', $node->getId())
                    ->setParameter('nestable_types', $this->nodeManager->getNestableNodeTypes());
            },
            'choice_label' => function ($node) {
                /** @var Node $node */
                return $node->getTranslation()->getTitle();
            },
            'required' => false,
        ]);
    }

    /**
     * @param NodeInterface $node
     * @return bool
     */
    public function isParentFieldRequired(?NodeInterface $node): bool
    {
        return null !== $node && null !== $node->getId();
    }
}
