<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Form\Subscriber;

use Omni\Sylius\CmsPlugin\Form\Type\NodeImageTranslationType;
use Omni\Sylius\CmsPlugin\Form\Type\ResourceAutocompleteChoiceType;
use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Manager\RelationManager;
use Omni\Sylius\CmsPlugin\Model\NodeImageInterface;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Sylius\Component\Resource\Model\ResourceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

/**
 * Adds all configurable fields to node images
 */
class NodeImageSubscriber implements EventSubscriberInterface
{
    /**
     * @var NodeTypeManager
     */
    protected $typeManager;

    /**
     * @var RelationManager
     */
    protected $relationManager;

    /**
     * @var NodeInterface
     */
    protected $node;

    /**
     * @param NodeTypeManager $typeManager
     * @param RelationManager $relationManager
     */
    public function __construct(
        NodeTypeManager $typeManager,
        RelationManager $relationManager
    ) {
        $this->typeManager = $typeManager;
        $this->relationManager = $relationManager;
    }

    /**
     * @param NodeInterface $node
     */
    public function setNode(NodeInterface $node): void
    {
        $this->node = $node;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::SUBMIT => 'onSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        $form = $event->getForm();

        if (!$this->node) {
            return;
        }

        $imagesConfig = $this->typeManager->getImagesConfiguration($this->node->getType());

        if (!$imagesConfig['enabled']) {
            return;
        }

        $this->addRelations($imagesConfig, $form, $event->getData());
        $this->addPositions($imagesConfig, $form);
        $this->addTranslations($imagesConfig, $form);
    }

    /**
     * @param FormEvent $event
     */
    public function onSubmit(FormEvent $event)
    {
        /** @var NodeImageInterface $node */
        $image = $event->getData();

        /** @var ResourceInterface $relation */
        if (null !== $relation = $image->getRelation()) {
            $image->setRelationId($relation->getId());
        }
    }

    /**
     * @param array $imagesConfig
     * @param FormInterface $form
     * @param null|NodeImageInterface $image
     */
    protected function addRelations(array $imagesConfig, FormInterface $form, ?NodeImageInterface $image): void
    {
        if (empty($imagesConfig['relations'])) {
            return;
        }

        // Loads the current relation so it can be displayed in the form
        if ($image && $image->getRelationId()) {
            $this->relationManager->getRelation($image);
        }

        $form
            ->add('relation_type', ChoiceType::class,
                [
                    'choices' => $this->formRelationSelectionArray($imagesConfig['relations']),
                    'multiple' => false,
                    'placeholder' => '-',
                    'required' => false,
                    'label' => 'omni_sylius.form.node_image.relation_type',
                ]
            )
            ->add('relation', ResourceAutocompleteChoiceType::class,
                [
                    'resource' => 'sylius.taxon',
                    'choice_name' => 'name',
                    'choice_value' => 'id'
                ]
            )
        ;
    }

    /**
     * @param array $imagesConfig
     * @param FormInterface $form
     */
    protected function addPositions(array $imagesConfig, FormInterface $form): void
    {
        if (count($imagesConfig['available_positions'])) {
            $form->add('position', ChoiceType::class, [
                'choices' => $imagesConfig['available_positions'],
                'placeholder' => '-',
                'required' => false,
            ]);
        }
    }

    /**
     * @param array $imagesConfig
     * @param FormInterface $form
     */
    protected function addTranslations(array $imagesConfig, FormInterface $form): void
    {
        if ($imagesConfig['title_aware']) {
            $form->add('translations', ResourceTranslationsType::class, [
                'entry_type' => NodeImageTranslationType::class,
                'label' => 'omni_sylius.form.node.translations',
            ]);
        }
    }

    /**
     * Forms the needed format of data array for relation type choice field:
     * ['relation_name' => 'relation_type', 'name' => 'type, ...]
     *
     * @param array $relationConfig
     *
     * @return array
     */
    private function formRelationSelectionArray(array $relationConfig): array
    {
        $resources = [];

        foreach ($relationConfig as $relation) {
            $resources[$relation['name']] = $relation['alias'];
        }

        return $resources;
    }
}
