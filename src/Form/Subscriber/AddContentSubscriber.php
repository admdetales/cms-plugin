<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Form\Subscriber;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeTranslationInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AddContentSubscriber implements EventSubscriberInterface
{
    /**
     * @var NodeTypeManager
     */
    private $nodeTypeManager;

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * AddContentSubscriber constructor.
     *
     * @param NodeTypeManager $nodeTypeManager
     */
    public function __construct($nodeTypeManager)
    {
        $this->nodeTypeManager = $nodeTypeManager;
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        /** @var NodeTranslationInterface $translation */
        if (null === $translation = $event->getData()) {
            return;
        }

        $type = $translation->getTranslatable()->getType();
        if ($this->nodeTypeManager->isSlugable($type)) {
            $event->getForm()
                ->add('slug', TextType::class, [
                    'label' => 'omni_sylius.form.node.slug',
                ])
            ;
        }
        if ($this->nodeTypeManager->hasContent($type)) {
            $event->getForm()
                ->add('content', TextareaType::class, [
                    'required' => false,
                    'label' => 'omni_sylius.form.node.description',
                    'attr' => ['class' => 'ckeditor'],
                ])
            ;
        }
    }
}
