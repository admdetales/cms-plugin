<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Form\Subscriber;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeTranslationInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AddLinkSubscriber implements EventSubscriberInterface
{
    /**
     * @var NodeTypeManager
     */
    protected $handler;

    /**
     * @var AddLinkToImageTranslationSubscriber
     */
    protected $addLinkToImageTranslationSubscriber;

    /**
     * @param NodeTypeManager $handler
     * @param AddLinkToImageTranslationSubscriber $addLinkToImageTranslationSubscriber
     */
    public function __construct(
        NodeTypeManager $handler,
        AddLinkToImageTranslationSubscriber $addLinkToImageTranslationSubscriber
    ) {
        $this->handler = $handler;
        $this->addLinkToImageTranslationSubscriber = $addLinkToImageTranslationSubscriber;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        /** @var NodeTranslationInterface $data */
        $data = $event->getData();
        if (false === $data instanceof NodeTranslationInterface) {
            return;
        }

        if ($this->handler->isLinkable($data->getTranslatable()->getType())) {
            $event->getForm()
                ->add(
                    'link',
                    TextType::class,
                    [
                        'label' => 'omni_sylius.form.node.link',
                        'required' => false,
                    ]
                )
                ->add(
                    'linkTarget',
                    ChoiceType::class,
                    [
                        'label' => 'omni_sylius.form.node.link_target',
                        'required' => false,
                        'choices' => [
                            'blank' => '_blank',
                            'parent' => '_parent',
                            'self' => '_self',
                            'top' => '_top',
                        ],
                        'choice_translation_domain' => false,
                    ]
                );
        }
    }
}
