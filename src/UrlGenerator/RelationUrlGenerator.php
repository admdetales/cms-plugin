<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\UrlGenerator;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Manager\RelationManager;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class RelationUrlGenerator implements RelationUrlGeneratorInterface
{
    /** @var RelationManager */
    private $relationManager;

    /** @var NodeTypeManager */
    private $typeManager;

    /** @var PropertyAccessorInterface */
    private $propertyAccessor;

    /** @var UrlGeneratorInterface */
    private $router;

    /**
     * @param RelationManager $relationManager
     * @param NodeTypeManager $typeManager
     * @param PropertyAccessorInterface $propertyAccessor
     * @param UrlGeneratorInterface $router
     */
    public function __construct(
        RelationManager $relationManager,
        NodeTypeManager $typeManager,
        PropertyAccessorInterface $propertyAccessor,
        UrlGeneratorInterface $router
    ) {
        $this->relationManager = $relationManager;
        $this->typeManager = $typeManager;
        $this->propertyAccessor = $propertyAccessor;
        $this->router = $router;
    }

    /**
     * @param NodeInterface $node
     *
     * @param int $referenceType
     *
     * @return string
     */
    public function generate(NodeInterface $node, int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string
    {
        $relation = $this->relationManager->getRelation($node);
        if (null === $relation) {
            throw new \RuntimeException('Node is without relation');
        }

        $route = $this->typeManager->getRelationRoute($node->getType());
        $routeParameters = $this->typeManager->getRelationRouteParameters($node->getType());

        foreach ($routeParameters as $key => $value) {
            if (preg_match('/^\$relation\..+/', $value)) {
                $routeParameters[$key] = $this->propertyAccessor->getValue($relation, substr($value, 10));
            }
        }

        $routeParameters['_locale'] = $node->getTranslation()->getLocale();

        return $this->router->generate($route, $routeParameters, $referenceType);
    }
}
